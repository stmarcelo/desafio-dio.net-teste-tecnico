﻿using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;

namespace dio.net_teste_tecnico.Models
{
    public class Pedido
    {
        public Pedido() { 
            Itens = new List<PedidoItem>();
        }

        [Key]
        public virtual int Id { get; set; }

        public virtual Status Status { get; set; }

        public virtual DateTime? Data { get; set; }
        
        public virtual Vendedor Vendedor { get; set; }

        public virtual List<PedidoItem> Itens { get; set; }

        public static Solicitacao Registrar(ModelsContext context, Pedido model)
        {
            var solicitacao = new Solicitacao();

            if(model.Itens == null || model.Itens.Count == 0)
            {
                solicitacao.Mensagem = "Informe pelo menos um item.";
                return solicitacao;
            }

            foreach(var item in model.Itens)
            {
                item.Pedido = model;
            }

            model.Data = DateTime.Now;
            model.Status = Status.AguardandoPagamento;

            context.Pedidos.Add(model);
            context.SaveChanges();

            solicitacao.Pedido = model;
            solicitacao.Sucesso = true;
            solicitacao.Mensagem = "Pedido registrado.";

            return solicitacao;

        }

        public static Solicitacao MudarStatus(ModelsContext context, int pedidoId, Status newStatus)
        {
            var solicitacao = new Solicitacao();

            var pedido = context.Pedidos
                .Where(c=>c.Id == pedidoId)
                .Include(c => c.Status)
                .Include(c=>c.Vendedor)
                .SingleOrDefault();
            if (pedido == null)
            {
                solicitacao.Mensagem = "Pedido não localizado.";
                return solicitacao;
            }

            if (pedido.Status.NextValid(newStatus))
            {
                pedido.Status = newStatus;
                context.Update(pedido);
                context.SaveChanges();

                solicitacao.Pedido = pedido;
                solicitacao.Sucesso = true;
                solicitacao.Mensagem = "Status alterado.";

                return solicitacao;
            }
            else
            {
                solicitacao.Mensagem = "Status não permitido.";
            }

            return solicitacao;
        }
    }
}
