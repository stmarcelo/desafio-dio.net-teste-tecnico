﻿namespace dio.net_teste_tecnico.Models
{
    public class Status
    {
        public virtual short Id { get; set; }

        public virtual string Descricao { get; set; }

        public virtual bool NextValid(Status newStatus)
        {
            if (this.Id == Status.AguardandoPagamento.Id)
            {
                if (newStatus.Id == Status.PagamentoAprovado.Id || newStatus.Id == Status.Cancelado.Id) return true;
            }else if (this.Id == Status.PagamentoAprovado.Id)
            {
                if (newStatus.Id == Status.EnviadoParaTransportadora.Id || newStatus.Id == Status.Cancelado.Id) return true;
            }
            else if (this.Id == Status.EnviadoParaTransportadora.Id)
            {
                if (newStatus.Id == Status.Entregue.Id) return true;
            }

            return false;
        }

        public static Status AguardandoPagamento
        {
            get { return new Status() {Id = 1, Descricao = "Aguardando Pagamento" }; }
        }

        public static Status PagamentoAprovado
        {
            get { return new Status() { Id = 2, Descricao = "Pagamento Aprovado" }; }
        }

        public static Status EnviadoParaTransportadora
        {
            get { return new Status() { Id = 3, Descricao = "Enviado para Transportadora" }; }
        }

        public static Status Entregue
        {
            get { return new Status() { Id = 4, Descricao = "Entregue" }; }
        }

        public static Status Cancelado
        {
            get { return new Status() { Id = 5, Descricao = "Cancelado" }; }
        }

        public static List<Status> All(ModelsContext context)
        {
            var list = context.Status.ToList();
            if (list.Count > 0) return list;
            
            return CreateStatus(context);
        }

        public static List<Status> CreateStatus(ModelsContext context)
        {
            context.Status.Add(Status.AguardandoPagamento);
            context.Status.Add(Status.PagamentoAprovado);
            context.Status.Add(Status.EnviadoParaTransportadora);
            context.Status.Add(Status.Entregue);
            context.Status.Add(Status.Cancelado);
            context.SaveChanges();

            return context.Status.ToList();
        }

    }
}
