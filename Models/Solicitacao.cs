﻿namespace dio.net_teste_tecnico.Models
{
    public class Solicitacao
    {

        public Pedido Pedido { get; set; }

        public bool Sucesso { get; set; }

        public string Mensagem { get; set; }
    }
}
