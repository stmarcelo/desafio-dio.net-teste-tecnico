﻿using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace dio.net_teste_tecnico.Models
{
    public class PedidoItem
    {
        [Key]
        public virtual int Id { get; set; }

        [JsonIgnore]
        public virtual Pedido? Pedido { get; set; }

        public virtual string Descricao { get; set; }

        public virtual int Qtde { get; set; }
    }
}
