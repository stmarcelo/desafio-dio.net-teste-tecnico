﻿namespace dio.net_teste_tecnico.Models
{
    public class Vendedor
    {
        public virtual int Id { get; set; }

        public virtual string CPF { get; set; }

        public virtual string Nome { get; set; }

        public virtual string Email { get; set; }

        public virtual string Telefone { get; set; }
    }
}
