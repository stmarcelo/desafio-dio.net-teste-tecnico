﻿using Microsoft.EntityFrameworkCore;

namespace dio.net_teste_tecnico.Models
{
    public class ModelsContext: DbContext
    {
        public ModelsContext(DbContextOptions<ModelsContext> options):base(options) { }

        public DbSet<Pedido> Pedidos { get; set; }

        public DbSet<PedidoItem> PedidoItens { get; set;}

        public DbSet<Vendedor> Vendedores { get; set; }

        public DbSet<Status> Status { get; set; }

        //protected override void OnModelCreating(ModelBuilder modelBuilder)
        //{
        //    base.OnModelCreating(modelBuilder);

        //    modelBuilder.Entity<Status>().HasData(
        //        Models.Status.AguardandoPagamento
        //        ,Models.Status.PagamentoAprovado
        //        ,Models.Status.EnviadoParaTransportadora
        //        ,Models.Status.Entregue
        //        ,Models.Status.Cancelado
        //    );
        //}
    }
}
