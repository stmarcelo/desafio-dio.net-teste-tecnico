﻿using dio.net_teste_tecnico.Models;
using Microsoft.AspNetCore.Mvc;

namespace dio.net_teste_tecnico.Controllers
{
    
    [ApiController]
    [Route("[controller]")]
    public class PedidoController : Controller
    {
        private readonly ModelsContext _context;

        public PedidoController(ModelsContext context)
        {
            _context = context;
            _context.Database.EnsureCreated();
        }

        [HttpPost("RegistrarVenda")]
        public Solicitacao Registrar(Pedido model)
        {
            return Pedido.Registrar(_context, model);
        }

        [HttpPost("MudarStatus")]
        public Solicitacao MudarStatus(int pedidoId, Status newStatus)
        {
            return Pedido.MudarStatus(_context, pedidoId, newStatus);
        }

    }
}
